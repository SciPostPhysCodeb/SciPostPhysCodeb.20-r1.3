dense() and sparse()
--------------------

.. _section:dense_and_sparse:

.. container:: mybox

   :class: note

   :py:class:`grassmanntn.dense`(**data**, **statistics**=None, **encoder**='canonical', **format**='standard')
   
   :py:class:`grassmanntn.sparse`(**data**, **statistics**=None, **encoder**='canonical', **format**='standard')

Create a Grassmann tensor object (dense or sparse) from a given initialization. :py:class:`dense`
is built upon the :py:class:`numpy.ndarray` class from the :py:mod:`numpy` package while :py:class:`sparse` is
built upon the :py:class:`sparse.COO` class from the :py:mod:`sparse` package. The two objects can
be used together in most cases, except with the arithmetic + and - operations.

**Parameters:**

- **data**: nested list, :py:class:`numpy.ndarray`, :py:class:`sparse.COO`, :py:class:`grassmanntn.dense`, or :py:class:`grassmanntn.sparse`
  This is the data object of the coefficient tensor.
  
- **statistics**: list or tuple of {0, 1, -1, '*'}
  A list of index statistics. The statistics is either +1 for a fermion, -1 for
  a conjugated fermion, 0 for a boson, or '*' for a hybrid index.
  If **data** is :py:class:`grassmanntn.dense` or :py:class:`grassmanntn.sparse`, this parameter is overwritten
  by **data.statistics**.
  
- **encoder**: {'canonical' (default), 'parity-preserving'}
  The encoding function of the initialized coefficient tensor.
  If **data** is :py:class:`grassmanntn.dense` or :py:class:`grassmanntn.sparse`, this parameter is overwritten
  by **data.encoder**.
  
- **format**: {'standard' (default), 'matrix'}
  The coefficient format of the initialized coefficient tensor.
  If **data** is :py:class:`grassmanntn.dense` or :py:class:`grassmanntn.sparse`, this parameter is overwritten
  by **data.format**.

**Returns:**

- **out**: :py:class:`grassmanntn.dense` or :py:class:`grassmanntn.sparse`
  A Grassmann tensor object satisfying the specified requirements.

Examples
--------

To make a random dense Grassmann tensor :math:`\\mathcal{T}_{\\psi\\bar\\phi\\bar\\zeta mn}` where $m$ and $n$ are bosonic indices with dimensions $d_\\psi=d_\\phi=4$, $d_\\zeta=8$, and $d_m=d_n=5$, the following command is used:

.. code-block:: python

   import numpy as np
   import grassmanntn as gtn
   T_data = np.random.rand(4, 4, 8, 5, 5)  # a random coeff with the specified shape.
   T_statistics = (1, -1, -1, 0, 0)  # the statistics of the indices.
   T = gtn.dense(data=T_data, statistics=T_statistics, encoder="canonical", format="standard")

Alternatively, the :py:func:`grassmanntn.random()` function can also be used instead:

.. code-block:: python

   T = gtn.random(shape=(4, 4, 8, 5, 5), statistics=(1, -1, -1, 0, 0), tensor_type=gtn.dense, dtype=float,
                  encoder="canonical", format="standard", skip_trimming=True)  # If False (default),
                                                                             # the Grassmann-odd components

Sparse Grassmann tensor can also be initialized in the COO (coordinate list) format if a list of non-zero entries is specified. For example, if one wants to initialize the following tensor (canonical and standard):

.. math::

   \\mathcal{T}_{\\bar\\psi\\phi} = 3.1 \\bar\\psi^3\\phi^5 + (7.9+2.3i) \\bar\\psi^2\\phi^7 + 5.8\\bar\\psi^0\\phi^1 -0.2i\\bar\\psi^2\\phi^2

where :math:`\\bar\\psi` and :math:`\\phi` are $2$- and $3$-bit fermions ($d_\\psi=4$ and $d_\\phi=8$), respectively, then we write:

.. code-block:: python

   import sparse as sp
   cI = complex(0, 1)
   T_shape = (4, 8)
   T_statistics = (-1, 1)
   psi_bar = [3, 2, 0, 2]  # psi_bar's index
   phi = [5, 7, 1, 2]  # phi's index
   coords = [psi_bar, phi]
   coeff = [3.1, 7.9 + 2.3 * cI, 5.8, -0.2 * cI]  # the coefficients
   T_data = sp.COO(coords, coeff, shape=T_shape)
   T = gtn.sparse(data=T_data, statistics=T_statistics, encoder="canonical", format="standard")

The two formats can be easily converted with:

.. code-block:: python

   T_sparse = gtn.sparse(T_dense)  # from dense to sparse
   T_dense = gtn.dense(T_sparse)  # from sparse to dense

Attributes
----------

- **data**: :py:class:`numpy.ndarray` for :py:class:`grassmanntn.dense`, :py:class:`sparse.COO` for :py:class:`grassmanntn.sparse`
  The coefficient tensor.
  
- **statistics**: tuple of {0, 1, -1, '*'}
  A list of index statistics.
  
- **encoder**: {'canonical', 'parity-preserving'}
  The encoding function of the created tensor.
  
- **format**: {'standard', 'matrix'}
  The coefficient format of the created tensor.
  
- **shape**: tuple of int
  A list of index dimensions of the created Grassmann tensor.
  
- **size**: int
  The number of all elements (including zeros), equals the product of dimensions.
  
- **ndim**: int
  The number of indices, equals the length of shape.
  
- **nnz**: int
  The number of nonzero elements. Nonzero elements are defined to be elements with norms larger than the parameter
  ``grassmanntn.numer_display_cutoff`` which has the default value of $10^{-11}$.
  Note that this *does not* mean that elements with smaller norms are eliminated.
  
- **norm**: float
  The Frobenius norm of the coefficient tensor

Methods
-------

- **display(name=None, indent_size=0)**
  Display all attributes of the tensor, as well as the list of all nonzero elements.
  The tensor name (``name``) and the display indent size (``indent_size``) are optional.

- **info(name=None, indent_size=0)**
  The same as ``display()`` but without showing the list of all nonzero elements.

- **remove_zeros()** (for :py:class:`grassmanntn.sparse` only)
  Remove all entries with norm less than the parameter ``grassmanntn.numer_cutoff`` which has the default value of $10^{-14}$.
  The original tensor is altered. Return nothing.
  
- **copy()**
  Return a deep copy of self.
  
- **switch_encoder()**
  Return a new tensor where the encoding function is switched. The original tensor is not altered. Does not work if there is a hybrid index.
  
- **force_encoder(target='canonical')**
  Return a new tensor where the encoding function is forced to be ``target``.
  The original tensor is not altered. Does not work if there is a hybrid index.
  
- **switch_format()**
  Return a new tensor where the coefficient format is switched. The original tensor is not altered. Does not work if there is a hybrid index.
  
- **force_format(target='standard')**
  Return a new tensor where the coefficient format is forced to be ``target``.
  The original tensor is not altered. Does not work if there is a hybrid index.
  
- **join_legs(*args)**
  Join the tensor legs with the prescription (see documentation for the prescription) and return the result.
  The original tensor is not altered. This is the same as calling the function
  :py:func:`grassmanntn.join_legs(self, *args)`.
  
- **split_legs(*args)**
  Split the tensor legs with the prescription (see documentation for the prescription) and return the result.
  The original tensor is not altered. This is the same as calling the function
  :py:func:`grassmanntn.split_legs(self, *args)`.
  
- **hconjugate(*args)**
  Return the Hermitian conjugate. The original tensor is not altered.
  This is the same as calling :py:func:`grassmanntn.hconjugate(self, *args)`.
  
- **svd(*args)**
  Singular value decomposition. Return the tuple $\mathcal{U}$, $\Sigma$, $\mathcal{V}$ in the documentation.
  This is the same as calling the function :py:func:`grassmanntn.svd(self, *args)`.
  
- **eig(*args)**
  Eigenvalue decomposition. Return the tuple $\mathcal{U}$, $\Sigma$, $\mathcal{U}^\dagger$. The tensor must be
  Hermitian for this to work correctly. Otherwise, use :py:func:`svd()` instead.
  This is the same as calling the function :py:func:`grassmanntn.eig(self, *args)`.

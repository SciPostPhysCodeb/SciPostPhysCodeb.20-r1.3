# Codebase release 1.3 for GrassmannTN

by Atis Yosprakob

SciPost Phys. Codebases 20-r1.3 (2023) - published 2023-11-16

[DOI:10.21468/SciPostPhysCodeb.20-r1.3](https://doi.org/10.21468/SciPostPhysCodeb.20-r1.3)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.20-r1.3) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Atis Yosprakob.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Live (external) repository at [https://github.com/ayosprakob/grassmanntn/releases/tag/v_133](https://github.com/ayosprakob/grassmanntn/releases/tag/v_133)
* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.20-r1.3](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.20-r1.3)
